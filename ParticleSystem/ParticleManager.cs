﻿using System;
using System.Collections.Generic;
using OpenTK;
using TarotMG.Engine;

namespace TarotMG.ParticleSystem
{
    class ParticleManager
    {
        public List<Particle> particles;

        private Texture2D particle1;
        private Texture2D particle2;
        private Texture2D particle3;

        private Random random;

        public ParticleManager()
        {
            particles = new List<Particle>();
            random = new Random();

            particle1 = Utilities.LoadTexture("particle1.png");
            particle2 = Utilities.LoadTexture("particle2.png");
            particle3 = Utilities.LoadTexture("particle3.png");
        }
        
        // функция, которая будет генерировать частицы
        public void EngineRocket(int x, int y, int loop = 1, int ttl_add = 0)
        {
            Vector2 position = new Vector2(x/2, y/2);

            // создаем 2 частицы дыма для трейла
            for (int a = 0; a < 2 * loop; a++)
            {
                Vector2 velocity = AngleToV2((float)(Math.PI * 2d * random.NextDouble()), 0.6f);
                float angle = 0;
                float angleVel = 0;
                Vector4 color = new Vector4(1f, 1f, 1f, 1f);
                float size = 3 * random.Next(1, 4);
                int ttl = 70 + ttl_add;
                float sizeVel = 0;
                float alphaVel = .1f;

                GenerateNewParticle(particle2, position, velocity, angle, angleVel, color, size, ttl, sizeVel, alphaVel);
            }

            // создаем 1 искру для трейла
            for (int a = 0; a < 1 * loop; a++)
            {
                Vector2 velocity = AngleToV2((float)(Math.PI * 2d * random.NextDouble()), .2f);
                float angle = 0;
                float angleVel = 0;
                Vector4 color = new Vector4(1.0f, 0.5f, 0.5f, 0.5f);
                float size = 3 * random.Next(1, 4);
                int ttl = 75 + ttl_add;
                float sizeVel = 0;
                float alphaVel = .1f;

                GenerateNewParticle(particle1, position, velocity, angle, angleVel, color, size, ttl, sizeVel, alphaVel);
            }

            for (int a = 0; a < 1 * loop; a++)
            {
                Vector2 velocity = AngleToV2((float)(Math.PI * 2d * random.NextDouble()), .1f);
                float angle = 0;
                float angleVel = 0;
                Vector4 color = new Vector4(1.0f, 0.5f, 0.5f, 0.5f);
                float size = 2 * random.Next(1, 4);
                int ttl = 50 + ttl_add;
                float sizeVel = 0;
                float alphaVel = .1f;

                GenerateNewParticle(particle3, position, velocity, angle, angleVel, color, size, ttl, sizeVel, alphaVel);
            }
        }

        // генерация новой частички
        private Particle GenerateNewParticle(Texture2D texture, Vector2 position, Vector2 velocity,
            float angle, float angularVelocity, Vector4 color, float size, int ttl, float sizeVel, float alphaVel)
        {
            Particle particle = new Particle(texture, position, velocity, angle, angularVelocity, color, size, ttl, sizeVel, alphaVel);
            particles.Add(particle);
            return particle;
        }

        public Vector2 AngleToV2(float angle, float length)
        {
            Vector2 direction = Vector2.Zero;
            direction.X = (float)Math.Cos(angle) * length;
            direction.Y = -(float)Math.Sin(angle) * length;
            return direction;
        }

        public void Update()
        {
            for (int particle = 0; particle < particles.Count; particle++)
            {
                particles[particle].Update();
                // если время жизни частички или её размеры равны нулю, удаляем её
                if (particles[particle].Size <= 0 || particles[particle].TTL <= 0)
                {
                    particles.RemoveAt(particle);
                    particle--;
                }
            }
        }

        public void Draw()
        {
            particles.ForEach(p => p.Draw());
        }
    }
}
