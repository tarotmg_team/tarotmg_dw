﻿using System;
using System.Linq;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace TarotMG.ParticleSystem
{
    class Particle
    {
        public Texture2D Texture { get; set; }        // Текстура нашей частички
        public Vector2 Position { get; set; }        // Позиция частички
        public Vector2 Velocity { get; set; }        // Скорость частички
        public float Angle { get; set; }            // Угол поворота частички
        public float AngularVelocity { get; set; }    // Угловая скорость
        public Vector4 Color { get; set; }            // Цвет частички
        public float Size { get; set; }                // Размеры
        public float SizeVel { get; set; }		// Скорость уменьшения размера
        public float AlphaVel { get; set; }		// Скорость уменьшения альфы
        public int TTL { get; set; }                // Время жизни частички

        private uint vertexBufferId;
        private uint textureBufferId;
        private float[] vertexData;

        // конструктор
        public Particle(Texture2D texture, Vector2 position, Vector2 velocity,
            float angle, float angularVelocity, Vector4 color, float size, int ttl, float sizeVel, float alphaVel)
        {
            Texture = texture;
            Position = position;
            Velocity = velocity;
            Angle = angle;
            Color = color;
            AngularVelocity = angularVelocity;
            Size = size;
            SizeVel = sizeVel;
            AlphaVel = alphaVel;
            TTL = ttl;

            #region SETUP VALUES
            // Set-up vertex buffer
            vertexData = new float[] {
                Position.X - Size/2f, Position.Y - Size/2f, 0.0f,      //LD
                Position.X + Size/2f, Position.Y - Size/2f, 0.0f,      //RD
                Position.X + Size/2f, Position.Y + Size/2f, 0.0f,      //RU
                Position.X - Size/2f, Position.Y + Size/2f, 0.0f,      //LU
            };

            // Set-up texture buffer
            float[] textureData = new float[] {
                0.0f, 0.0f, // LU
                1.0f, 0.0f, // RU
                1.0f, 1.0f, // RD
                0.0f, 1.0f, // LD
            };
            #endregion
            #region GENERATE BUFFERS
            // Vertex buffer generate
            GL.GenBuffers(1, out vertexBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertexData.Length * sizeof(float)), vertexData, BufferUsageHint.StaticDraw);
            // Texture buffer generate
            GL.GenBuffers(1, out textureBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, textureBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(textureData.Length * sizeof(float)), textureData, BufferUsageHint.StaticDraw);
            #endregion
        }

        public void Update()
        {
            TTL--; // уменьшаем время жизни

            // Меняем параметры в соответствии с скоростями
            Position += Velocity;
            Angle += AngularVelocity;
            Size += SizeVel;

            // убавляем цвет. Кстати, цвет записан в Vector4, а не в Color, потому что: Color.R/G/B имеет тип Byte (от 0x00 до 0xFF),
            // чтобы не проделывать лишней трансформации, используем float и Vector4
            Color = new Vector4(Color.X, Color.Y, Color.Z, Color.W - AlphaVel);
        }

        public void Draw()
        {
            //Rectangle sourceRectangle = new Rectangle(0, 0, Texture.Width, Texture.Height); // область из текстуры: вся
            //Vector2 origin = new Vector2(Texture.Width / 2, Texture.Height / 2); // центр
            //spriteBatch.Draw(Texture, Position, sourceRectangle, new Color(Color), Angle, origin, Size, SpriteEffects.None, 0); // акт прорисовки
            GL.LoadIdentity();

            GL.Translate(new Vector3(Position.X, Position.Y, 0));
            GL.Rotate(Angle, Vector3.UnitZ);

            Texture.Bind();
            EnableStates();
            #region BIND BUFFERS
            // Bind vertex buffer
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.VertexPointer(3, VertexPointerType.Float, 0, IntPtr.Zero);
            // Bind texture buffer
            GL.BindBuffer(BufferTarget.ArrayBuffer, textureBufferId);
            GL.TexCoordPointer(2, TexCoordPointerType.Float, 0, IntPtr.Zero);
            #endregion
            GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
            DisableStates();
            Texture.Unbind();
        }

        private void EnableStates()
        {
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
            GL.Enable(EnableCap.AlphaTest);
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);
        }

        private void DisableStates()
        {
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);
            GL.Disable(EnableCap.AlphaTest);
            GL.Disable(EnableCap.Blend);
        }
    }
}
