﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using TarotMG.Engine;

namespace TarotMG.GUI
{
    class GuiContextMenu : IGuiObject
    {
        private uint vertexBufferId;
        private uint textureBufferId;
        private float[] vertexData;
        private string texture_path_0;
        private string texture_path_1;
        private string texture_path_2;
        private string texture_path_3;
        private string texture_path_4;
        private string texture_path_5;
        private string texture_path_6;
        private string texture_path_7;
        private Texture2D texture0;
        private Texture2D texture1;
        private Texture2D texture2;
        private Texture2D texture3;
        private Texture2D texture4;
        private Texture2D texture5;
        private Texture2D texture6;
        private Texture2D texture7;
        private Vector3 position0;
        private Vector3 position1;
        private Vector3 position2;
        private Vector3 position3;
        private Vector3 position4;
        private Vector3 position5;
        private Vector3 position6;
        private Vector3 position7;
        private Size client_size;

        public string Name { get; set; }
        public Vector2 Position { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool IsVisible { get; set; }
        public int SelectedID = -1;

        private float margin = 4;
        private bool margin_bool = false;

        public GuiContextMenu(string name, Size clientsize, int width, int height, bool visible = false)
        {
            Name = name;
            Width = width;
            Height = height;
            IsVisible = visible;
            client_size = clientsize;

            SetTexturesPath();
            LoadTextures();
            SetTexturesPositions();

            Position = Vector2.Zero;
            #region SETUP VALUES
            // Set-up vertex buffer
            vertexData = new float[] {
                Position.X - Width/2f, Position.Y - Height/2f, 0.0f,    //LD
                Position.X + Width/2f, Position.Y - Height/2f, 0.0f,    //RD
                Position.X + Width/2f, Position.Y + Height/2f, 0.0f,    //RU
                Position.X - Width/2f, Position.Y + Height/2f, 0.0f,    //LU
            };

            // Set-up texture buffer
            float[] textureData = new float[] {
                0.0f, 0.0f, // LU
                1.0f, 0.0f, // RU
                1.0f, 1.0f, // RD
                0.0f, 1.0f, // LD
            };
            #endregion
            #region GENERATE BUFFERS
            // Vertex buffer generate
            GL.GenBuffers(1, out vertexBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertexData.Length * sizeof(float)), vertexData, BufferUsageHint.StaticDraw);
            // Texture buffer generate
            GL.GenBuffers(1, out textureBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, textureBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(textureData.Length * sizeof(float)), textureData, BufferUsageHint.StaticDraw);
            #endregion
            Position = new Vector2(client_size.Width / 2, client_size.Height / 2);
        }

        private void SetTexturesPath()
        {
            texture_path_0 = "gui/gui-context-menu-0.png";
            texture_path_1 = "gui/gui-context-menu-1.png";
            texture_path_2 = "gui/gui-context-menu-2.png";
            texture_path_3 = "gui/gui-context-menu-3.png";
            texture_path_4 = "gui/gui-context-menu-4.png";
            texture_path_5 = "gui/gui-context-menu-5.png";
            texture_path_6 = "gui/gui-context-menu-6.png";
            texture_path_7 = "gui/gui-context-menu-7.png";
        }
        private void LoadTextures()
        {
            texture0 = Utilities.LoadTexture(texture_path_0);
            texture1 = Utilities.LoadTexture(texture_path_1);
            texture2 = Utilities.LoadTexture(texture_path_2);
            texture3 = Utilities.LoadTexture(texture_path_3);
            texture4 = Utilities.LoadTexture(texture_path_4);
            texture5 = Utilities.LoadTexture(texture_path_5);
            texture6 = Utilities.LoadTexture(texture_path_6);
            texture7 = Utilities.LoadTexture(texture_path_7);
        }
        private void SetTexturesPositions()
        {
            position0 = new Vector3(Position.X - Width - margin, Position.Y - Height - margin, 0);
            position1 = new Vector3(Position.X, Position.Y - Height - margin, 0);
            position2 = new Vector3(Position.X + Width + margin, Position.Y - Height - margin, 0);
            position3 = new Vector3(Position.X + Width + margin, Position.Y, 0);
            position4 = new Vector3(Position.X + Width + margin, Position.Y + Height + margin, 0);
            position5 = new Vector3(Position.X, Position.Y + Height + margin, 0);
            position6 = new Vector3(Position.X - Width - margin, Position.Y + Height + margin, 0);
            position7 = new Vector3(Position.X - Width - margin, Position.Y, 0);
        }
        private void DeleteTextures()
        {
            GL.DeleteTexture(texture0.ID);
            GL.DeleteTexture(texture1.ID);
            GL.DeleteTexture(texture2.ID);
            GL.DeleteTexture(texture3.ID);
            GL.DeleteTexture(texture4.ID);
            GL.DeleteTexture(texture5.ID);
            GL.DeleteTexture(texture6.ID);
            GL.DeleteTexture(texture7.ID);
        }
        
        public void Resize(int width, int height)
        {
            client_size.Width = width;
            client_size.Height = height;

            DeleteTextures();
            LoadTextures();

            Position = Vector2.Zero;

            #region SETUP VALUES
            // Set-up vertex buffer
            vertexData = new float[] {
                Position.X - Width/2f, Position.Y - Height/2f, 0.0f,    //LD
                Position.X + Width/2f, Position.Y - Height/2f, 0.0f,    //RD
                Position.X + Width/2f, Position.Y + Height/2f, 0.0f,    //RU
                Position.X - Width/2f, Position.Y + Height/2f, 0.0f,    //LU
            };

            // Set-up texture buffer
            float[] textureData = new float[] {
                0.0f, 0.0f, // LU
                1.0f, 0.0f, // RU
                1.0f, 1.0f, // RD
                0.0f, 1.0f, // LD
            };
            #endregion
            #region GENERATE BUFFERS
            // Vertex buffer generate
            GL.GenBuffers(1, out vertexBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertexData.Length * sizeof(float)), vertexData, BufferUsageHint.StaticDraw);
            // Texture buffer generate
            GL.GenBuffers(1, out textureBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, textureBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(textureData.Length * sizeof(float)), textureData, BufferUsageHint.StaticDraw);
            #endregion

            Position = new Vector2(client_size.Width / 2, client_size.Height / 2);
        }
        public void Update(float time)
        {
            if (IsVisible)
            {
                if (margin <= 5) margin_bool = true;
                else if (margin >= 10) margin_bool = false;
                if (margin_bool) margin += 4.5f * time;
                else margin -= 4.5f * time;

                SetTexturesPositions();
            }
            else
            {
                SelectedID = -1;
            }
        }
        public void Draw()
        {
            if (IsVisible)
            {
                // Bind texture buffer
                GL.BindBuffer(BufferTarget.ArrayBuffer, textureBufferId);
                GL.TexCoordPointer(2, TexCoordPointerType.Float, 0, IntPtr.Zero);
                // Bind vertex buffer
                GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
                GL.VertexPointer(3, VertexPointerType.Float, 0, IntPtr.Zero);
                EnableStates();

                GL.LoadIdentity();
                GL.Translate(position0);
                texture0.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                texture0.Unbind();

                GL.LoadIdentity();
                GL.Translate(position1);
                texture1.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                texture1.Unbind();

                GL.LoadIdentity();
                GL.Translate(position2);
                texture2.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                texture2.Unbind();

                GL.LoadIdentity();
                GL.Translate(position3);
                texture3.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                texture3.Unbind();

                GL.LoadIdentity();
                GL.Translate(position4);
                texture4.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                texture4.Unbind();

                GL.LoadIdentity();
                GL.Translate(position5);
                texture5.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                texture5.Unbind();

                GL.LoadIdentity();
                GL.Translate(position6);
                texture6.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                texture6.Unbind();

                GL.LoadIdentity();
                GL.Translate(position7);
                texture7.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                texture7.Unbind();

                DisableStates();
            }
        }

        private void EnableStates()
        {
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
            GL.Enable(EnableCap.AlphaTest);
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);
        }
        private void DisableStates()
        {
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);
            GL.Disable(EnableCap.AlphaTest);
            GL.Disable(EnableCap.Blend);
        }

        public bool IntersectionTest(int x, int y)
        {
            List<RectangleF> rectList = new List<RectangleF>();

            rectList.Add(new RectangleF(position0.X - Width / 2, position0.Y - Height / 2, Width, Height));
            rectList.Add(new RectangleF(position1.X - Width / 2, position1.Y - Height / 2, Width, Height));
            rectList.Add(new RectangleF(position2.X - Width / 2, position2.Y - Height / 2, Width, Height));
            rectList.Add(new RectangleF(position3.X - Width / 2, position3.Y - Height / 2, Width, Height));
            rectList.Add(new RectangleF(position4.X - Width / 2, position4.Y - Height / 2, Width, Height));
            rectList.Add(new RectangleF(position5.X - Width / 2, position5.Y - Height / 2, Width, Height));
            rectList.Add(new RectangleF(position6.X - Width / 2, position6.Y - Height / 2, Width, Height));
            rectList.Add(new RectangleF(position7.X - Width / 2, position7.Y - Height / 2, Width, Height));

            foreach (var rect in rectList)
            {
                if (rect.Contains(x,y))
                {
                    SelectedID = rectList.IndexOf(rect);
                    return rect.Contains(x, y);
                }
            }

            SelectedID = -1;
            return false;
        }
    }
}
