﻿using OpenTK;

namespace TarotMG.GUI
{
    interface IGuiObject
    {
        string Name { get; set; }
        Vector2 Position { get; set; }
        int Width { get; set; }
        int Height { get; set; }
        bool IsVisible { get; set; }
        
        void Resize(int width, int height);
        void Update(float time);
        void Draw();
    }
}
