﻿using System;
using System.Drawing;
using System.Linq;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using TarotMG.Engine;

namespace TarotMG.GUI
{
    class GuiLoadingIcon : IGuiObject
    {
        private uint vertexBufferId;
        private uint backgroundVertexBufferId;
        private uint textureBufferId;
        private float[] vertexData;
        private float[] backgroundVertexData;
        private readonly string texture_path_0;
        private readonly string texture_path_1;
        private readonly string texture_path_2;
        private readonly string texture_path_3;
        private readonly string texture_path_4;
        private readonly string texture_path_5;
        private readonly string texture_path_6;
        private Texture2D texture0;
        private Texture2D texture1;
        private Texture2D texture2;
        private Texture2D texture3;
        private Texture2D texture4;
        private Texture2D texture5;
        private Texture2D texture6;
        private float angle1;
        private float angle2;
        private float angle3;
        private float angle4;
        private float angle5;
        private float angle6;
        private Size client_size;

        public string Name { get; set; }
        public Vector2 Position { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool IsVisible { get; set; }

        public GuiLoadingIcon(string name, Size clientsize, int width, int height, bool visible = false)
        {
            Name = name;
            Width = width;
            Height = height;
            IsVisible = visible;
            client_size = clientsize;
            texture_path_0 = "gui/gui-load-0.png";
            texture_path_1 = "gui/gui-load-1.png";
            texture_path_2 = "gui/gui-load-2.png";
            texture_path_3 = "gui/gui-load-3.png";
            texture_path_4 = "gui/gui-load-4.png";
            texture_path_5 = "gui/gui-load-5.png";
            texture_path_6 = "gui/gui-load-6.png";
            texture0 = Utilities.LoadTexture(texture_path_0);
            texture1 = Utilities.LoadTexture(texture_path_1);
            texture2 = Utilities.LoadTexture(texture_path_2);
            texture3 = Utilities.LoadTexture(texture_path_3);
            texture4 = Utilities.LoadTexture(texture_path_4);
            texture5 = Utilities.LoadTexture(texture_path_5);
            texture6 = Utilities.LoadTexture(texture_path_6);

            Position = Vector2.Zero;
            #region SETUP VALUES
            // Set-up background vertex buffer
            backgroundVertexData = new float[] {
                0, 0, 0,                                //LD
                client_size.Width, 0, 0,                 //RD
                client_size.Width, client_size.Height, 0, //RU
                0, client_size.Height, 0,                //LU
            };
            // Set-up vertex buffer
            vertexData = new float[] {
                Position.X - Width/2f, Position.Y - Height/2f, 0.0f,    //LD
                Position.X + Width/2f, Position.Y - Height/2f, 0.0f,    //RD
                Position.X + Width/2f, Position.Y + Height/2f, 0.0f,    //RU
                Position.X - Width/2f, Position.Y + Height/2f, 0.0f,    //LU
            };

            // Set-up texture buffer
            float[] textureData = new float[] {
                0.0f, 0.0f, // LU
                1.0f, 0.0f, // RU
                1.0f, 1.0f, // RD
                0.0f, 1.0f, // LD
            };
            #endregion
            #region GENERATE BUFFERS
            // Vertex buffer generate
            GL.GenBuffers(1, out vertexBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertexData.Length * sizeof(float)), vertexData, BufferUsageHint.StaticDraw);
            // Background vertex buffer generate
            GL.GenBuffers(1, out backgroundVertexBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, backgroundVertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(backgroundVertexData.Length * sizeof(float)), backgroundVertexData, BufferUsageHint.StaticDraw);
            // Texture buffer generate
            GL.GenBuffers(1, out textureBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, textureBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(textureData.Length * sizeof(float)), textureData, BufferUsageHint.StaticDraw);
            #endregion
            Position = new Vector2(client_size.Width/2, client_size.Height/2);
        }
        
        public void Resize(int width, int height)
        {
            client_size.Width = width;
            client_size.Height = height;

            GL.DeleteTexture(texture0.ID);
            GL.DeleteTexture(texture1.ID);
            GL.DeleteTexture(texture2.ID);
            GL.DeleteTexture(texture3.ID);
            GL.DeleteTexture(texture4.ID);
            GL.DeleteTexture(texture5.ID);
            GL.DeleteTexture(texture6.ID);

            texture0 = Utilities.LoadTexture(texture_path_0);
            texture1 = Utilities.LoadTexture(texture_path_1);
            texture2 = Utilities.LoadTexture(texture_path_2);
            texture3 = Utilities.LoadTexture(texture_path_3);
            texture4 = Utilities.LoadTexture(texture_path_4);
            texture5 = Utilities.LoadTexture(texture_path_5);
            texture6 = Utilities.LoadTexture(texture_path_6);
            
            Position = Vector2.Zero;

            #region SETUP VALUES
            // Set-up background vertex buffer
            backgroundVertexData = new float[] {
                0, 0, 0,                                //LD
                client_size.Width, 0, 0,                 //RD
                client_size.Width, client_size.Height, 0, //RU
                0, client_size.Height, 0,                //LU
            };
            // Set-up vertex buffer
            vertexData = new float[] {
                Position.X - Width/2f, Position.Y - Height/2f, 0.0f,    //LD
                Position.X + Width/2f, Position.Y - Height/2f, 0.0f,    //RD
                Position.X + Width/2f, Position.Y + Height/2f, 0.0f,    //RU
                Position.X - Width/2f, Position.Y + Height/2f, 0.0f,    //LU
            };

            // Set-up texture buffer
            float[] textureData = new float[] {
                0.0f, 0.0f, // LU
                1.0f, 0.0f, // RU
                1.0f, 1.0f, // RD
                0.0f, 1.0f, // LD
            };
            #endregion
            #region GENERATE BUFFERS
            // Vertex buffer generate
            GL.GenBuffers(1, out vertexBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertexData.Length * sizeof(float)), vertexData, BufferUsageHint.StaticDraw);
            // Background vertex buffer generate
            GL.GenBuffers(1, out backgroundVertexBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, backgroundVertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(backgroundVertexData.Length * sizeof(float)), backgroundVertexData, BufferUsageHint.StaticDraw);
            // Texture buffer generate
            GL.GenBuffers(1, out textureBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, textureBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(textureData.Length * sizeof(float)), textureData, BufferUsageHint.StaticDraw);
            #endregion

            Position = new Vector2(client_size.Width / 2, client_size.Height / 2);
        }
        public void Update(float time)
        {
            if (IsVisible)
            {
                if (angle1 > 360) angle1 = 0; angle1 += 72 * time;
                if (angle2 > 360) angle2 = 0; angle2 += 72 * time;
                if (angle3 > 360) angle3 = 0; angle3 += 36 * time;
                if (angle4 > 360) angle4 = 0; angle4 += 36 * time;
                if (angle5 > 360) angle5 = 0; angle5 += 148 * time;
                if (angle6 > 360) angle6 = 0; angle6 += 148 * time;
            }
        }
        public void Draw()
        {
            if (IsVisible)
            {
                // Bind texture buffer
                GL.BindBuffer(BufferTarget.ArrayBuffer, textureBufferId);
                GL.TexCoordPointer(2, TexCoordPointerType.Float, 0, IntPtr.Zero);

                // Bind background vertex buffer
                GL.BindBuffer(BufferTarget.ArrayBuffer, backgroundVertexBufferId);
                GL.VertexPointer(3, VertexPointerType.Float, 0, IntPtr.Zero);

                EnableStates();
                GL.LoadIdentity();
                texture0.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                texture0.Unbind();

                // Bind vertex buffer
                GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
                GL.VertexPointer(3, VertexPointerType.Float, 0, IntPtr.Zero);

                GL.LoadIdentity();
                GL.Translate(new Vector3(Position.X, Position.Y, 0));
                GL.Rotate(angle1, Vector3.UnitZ);
                texture1.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                texture1.Unbind();

                GL.LoadIdentity();
                GL.Translate(new Vector3(Position.X, Position.Y, 0));
                GL.Rotate(-angle2, Vector3.UnitZ);
                texture2.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                texture2.Unbind();

                GL.LoadIdentity();
                GL.Translate(new Vector3(Position.X, Position.Y, 0));
                GL.Rotate(angle3, Vector3.UnitZ);
                texture3.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                texture3.Unbind();

                GL.LoadIdentity();
                GL.Translate(new Vector3(Position.X, Position.Y, 0));
                GL.Rotate(-angle4, Vector3.UnitZ);
                texture4.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                texture4.Unbind();

                GL.LoadIdentity();
                GL.Translate(new Vector3(Position.X, Position.Y, 0));
                GL.Rotate(angle5, Vector3.UnitZ);
                texture5.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                texture5.Unbind();

                GL.LoadIdentity();
                GL.Translate(new Vector3(Position.X, Position.Y, 0));
                GL.Rotate(-angle6, Vector3.UnitZ);
                texture6.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                texture6.Unbind();
                DisableStates();
            }
        }
        private void EnableStates()
        {
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
            GL.Enable(EnableCap.AlphaTest);
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);
        }
        private void DisableStates()
        {
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);
            GL.Disable(EnableCap.AlphaTest);
            GL.Disable(EnableCap.Blend);
        }
    }
}
