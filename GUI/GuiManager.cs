﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TarotMG.GUI
{
    class GuiManager
    {
        public static List<IGuiObject> GuiList = new List<IGuiObject>();

        public static void Update(float time)
        {
            GuiList.ForEach(g => g.Update(time));
        }

        public static void WindowResize(int width, int height)
        {
            GuiList.Find(l => l.Name == "loading_icon").Resize(width, height);
        }

        public static int Click(int x, int y)
        {
            GuiContextMenu gcm = (GuiContextMenu)GuiList.Find(l => l.Name == "context_menu");
            if (gcm.IsVisible)
            {
                gcm.IntersectionTest(x, y);
            }

            return gcm.SelectedID;
        }

        public static void HideContextMenu()
        {
            GuiContextMenu gcm = (GuiContextMenu)GuiList.Find(l => l.Name == "context_menu");
            gcm.IsVisible = false;
        }

        public static void Draw()
        {
            GuiList.FindAll(g => g.IsVisible == true).ForEach(g => g.Draw());
        }
    }
}
