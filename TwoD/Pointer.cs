﻿using System;
using System.Linq;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using TarotMG.Engine;

namespace TarotMG.TwoD
{
    class Pointer
    {
        private uint vertexBufferId;
        private uint textureBufferId;
        private float[] vertexData;
        private string texture_path_0;
        private string texture_path_1;
        private Texture2D texture0;
        private Texture2D texture1;
        private Texture2D currentTexture;
        private int Width;
        private int Height;
        public Vector2 Position { get; private set; } = Vector2.Zero;
        public bool IsVisible = false;

        public Pointer(int x, int y, int width, int height)
        {
            Width = width;
            Height = height;

            SetTexturesPath();
            LoadTextures();

            Position = new Vector2(x, y);
            currentTexture = texture0;

            #region SETUP VALUES
            // Set-up vertex buffer
            vertexData = new float[] {
                0.0f, 0.0f, 0.0f,   //LD
                Width, 0.0f, 0.0f,  //RD
                Width, Height, 0.0f,//RU
                0.0f, Height, 0.0f  //LU
            };

            // Set-up texture buffer
            float[] textureData = new float[] {
                0.0f, 0.0f, // LU
                1.0f, 0.0f, // RU
                1.0f, 1.0f, // RD
                0.0f, 1.0f, // LD
            };
            #endregion
            #region GENERATE BUFFERS
            // Vertex buffer generate
            GL.GenBuffers(1, out vertexBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertexData.Length * sizeof(float)), vertexData, BufferUsageHint.StaticDraw);
            // Texture buffer generate
            GL.GenBuffers(1, out textureBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, textureBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(textureData.Length * sizeof(float)), textureData, BufferUsageHint.StaticDraw);
            #endregion
        }
        public void Draw()
        {
            if (!IsVisible)
            {
                #region BIND BUFFERS
                // Bind vertex buffer
                GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
                GL.VertexPointer(3, VertexPointerType.Float, 0, IntPtr.Zero);
                // Bind texture buffer
                GL.BindBuffer(BufferTarget.ArrayBuffer, textureBufferId);
                GL.TexCoordPointer(2, TexCoordPointerType.Float, 0, IntPtr.Zero);
                #endregion
                EnableStates();

                GL.LoadIdentity();
                GL.Translate(new Vector3(Position.X, Position.Y, 0));
                currentTexture.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                currentTexture.Unbind();

                DisableStates();
            }
        }
        public void OnMouseDown()
        {
            currentTexture = texture1;
        }
        public void OnMouseUp()
        {
            currentTexture = texture0;
        }
        public void OnMouseMove(int x, int y, int client_width, int client_height, WindowState ws)
        {
            Position = new Vector2(x, y);
            if (ws != WindowState.Fullscreen)
            {
                IsVisible = false;
                int margin = 3;
                if (x < margin || x > client_width - margin || y < margin || y > client_height - margin)
                {
                    IsVisible = true;
                }
            }
            else
            {
                IsVisible = false;
            }
        }
        private void SetTexturesPath()
        {
            texture_path_0 = "cursor/cursor-0.png";
            texture_path_1 = "cursor/cursor-1.png";
        }
        private void LoadTextures()
        {
            texture0 = Utilities.LoadTexture(texture_path_0);
            texture1 = Utilities.LoadTexture(texture_path_1);
        }
        private void DeleteTextures()
        {
            GL.DeleteTexture(texture0.ID);
            GL.DeleteTexture(texture1.ID);
        }
        private void EnableStates()
        {
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
            GL.Enable(EnableCap.AlphaTest);
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);
        }
        private void DisableStates()
        {
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);
            GL.Disable(EnableCap.AlphaTest);
            GL.Disable(EnableCap.Blend);
        }
    }
}
