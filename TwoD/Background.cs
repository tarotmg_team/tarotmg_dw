﻿using System;
using System.Linq;
using OpenTK.Graphics.OpenGL;
using TarotMG.Engine;

namespace TarotMG.TwoD
{
    class Background
    {
        private uint vertexBufferId;
        private uint textureBufferId;
        private float[] vertexData;
        private string texture_path;
        private Texture2D texture;

        public Background(int width, int height)
        {
            texture_path = "background.png";
            texture = Utilities.LoadTexture(texture_path);
            #region SETUP VALUES
            // Set-up vertex buffer
            vertexData = new float[] {
                0.0f, 0.0f, 0.0f,
                width, 0.0f, 0.0f,
                width, height, 0.0f,
                0.0f, height, 0.0f
            };

            // Set-up texture buffer
            float[] textureData = new float[] {
                0.0f, 0.0f, // LU
                1.0f, 0.0f, // RU
                1.0f, 1.0f, // RD
                0.0f, 1.0f, // LD
            };
            #endregion
            #region GENERATE BUFFERS
            // Vertex buffer generate
            GL.GenBuffers(1, out vertexBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertexData.Length * sizeof(float)), vertexData, BufferUsageHint.StaticDraw);
            // Texture buffer generate
            GL.GenBuffers(1, out textureBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, textureBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(textureData.Length * sizeof(float)), textureData, BufferUsageHint.StaticDraw);
            #endregion
        }

        public void Resize(int width, int height)
        {
            GL.DeleteTexture(texture.ID);
            texture = Utilities.LoadTexture(texture_path);
            #region SETUP VALUES
            // Set-up vertex buffer
            vertexData = new float[] {
                0.0f, 0.0f, 0.0f,
                width, 0.0f, 0.0f,
                width, height, 0.0f,
                0.0f, height, 0.0f,
            };
            #endregion
            #region GENERATE BUFFERS
            // Vertex buffer generate
            GL.GenBuffers(1, out vertexBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertexData.Length * sizeof(float)), vertexData, BufferUsageHint.StaticDraw);
            #endregion
        }

        public void Draw()
        {
            texture.Bind();
            EnableStates();
            #region BIND BUFFERS
            // Bind vertex buffer
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.VertexPointer(3, VertexPointerType.Float, 0, IntPtr.Zero);
            // Bind texture buffer
            GL.BindBuffer(BufferTarget.ArrayBuffer, textureBufferId);
            GL.TexCoordPointer(2, TexCoordPointerType.Float, 0, IntPtr.Zero);
            #endregion
            GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
            DisableStates();
            texture.Unbind();
        }

        private void EnableStates()
        {
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);
        }

        private void DisableStates()
        {
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);
        }
    }
}
