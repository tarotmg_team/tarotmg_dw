﻿using System;
using System.Linq;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using TarotMG.Engine;

namespace TarotMG.TwoD
{
    class Watermark
    {
        private uint vertexBufferId;
        private uint textureBufferId;
        private float[] vertexData;
        private string texture_path;
        private Texture2D texture;
        private Vector2 pos;
        private int w, h;
        private float angle = 0;
        public bool IsVisible = true;
        public bool IsRotate = false;

        public Watermark(float x, float y, int width, int height)
        {
            w = width;
            h = height;
            texture_path = "watermark.png";
            texture = Utilities.LoadTexture(texture_path);

            pos = Vector2.Zero;

            #region SETUP VALUES
            // Set-up vertex buffer
            vertexData = new float[] {
                pos.X - width/2f, pos.Y - height/2f, 0.0f,      //LD
                pos.X + width/2f, pos.Y - height/2f, 0.0f,      //RD
                pos.X + width/2f, pos.Y + height/2f, 0.0f,      //RU
                pos.X - width/2f, pos.Y + height/2f, 0.0f,      //LU
            };

            // Set-up texture buffer
            float[] textureData = new float[] {
                0.0f, 0.0f, // LU
                1.0f, 0.0f, // RU
                1.0f, 1.0f, // RD
                0.0f, 1.0f, // LD
            };
            #endregion
            #region GENERATE BUFFERS
            // Vertex buffer generate
            GL.GenBuffers(1, out vertexBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertexData.Length * sizeof(float)), vertexData, BufferUsageHint.StaticDraw);
            // Texture buffer generate
            GL.GenBuffers(1, out textureBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, textureBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(textureData.Length * sizeof(float)), textureData, BufferUsageHint.StaticDraw);
            #endregion

            pos = new Vector2(x, y);
        }

        public void Resize(int width, int height)
        {
            GL.DeleteTexture(texture.ID);
            texture = Utilities.LoadTexture(texture_path);

            Vector2 tmp = pos;
            pos = Vector2.Zero;

            #region SETUP VALUES
            // Set-up vertex buffer
            vertexData = new float[] {
                pos.X - width/2f, pos.Y - height/2f, 0.0f,      //LD
                pos.X + width/2f, pos.Y - height/2f, 0.0f,      //RD
                pos.X + width/2f, pos.Y + height/2f, 0.0f,      //RU
                pos.X - width/2f, pos.Y + height/2f, 0.0f,      //LU
            };
            #endregion
            #region GENERATE BUFFERS
            // Vertex buffer generate
            GL.GenBuffers(1, out vertexBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertexData.Length * sizeof(float)), vertexData, BufferUsageHint.StaticDraw);
            #endregion

            pos = tmp;
        }

        public void Update(float time)
        {
            if (IsRotate)
            {
                if (angle > 360) angle = 0;
                angle += 36f * time;
            }
        }

        public void Draw()
        {
            if (IsVisible)
            {
                GL.LoadIdentity();

                GL.Translate(new Vector3(pos.X, pos.Y, 0));
                if (IsRotate) GL.Rotate(angle, Vector3.UnitZ);

                texture.Bind();
                EnableStates();
                #region BIND BUFFERS
                // Bind vertex buffer
                GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
                GL.VertexPointer(3, VertexPointerType.Float, 0, IntPtr.Zero);
                // Bind texture buffer
                GL.BindBuffer(BufferTarget.ArrayBuffer, textureBufferId);
                GL.TexCoordPointer(2, TexCoordPointerType.Float, 0, IntPtr.Zero);
                #endregion
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                DisableStates();
                texture.Unbind();
            }
        }

        public void SetPosition(Vector2 position)
        {
            pos = position;
            Resize(w, h);
        }

        private void EnableStates()
        {
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
            GL.Enable(EnableCap.AlphaTest);
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);
        }

        private void DisableStates()
        {
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);
            GL.Disable(EnableCap.AlphaTest);
            GL.Disable(EnableCap.Blend);
        }
    }
}
