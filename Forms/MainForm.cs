﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Input;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using TarotMG.GUI;
using TarotMG.ParticleSystem;
using TarotMG.TwoD;
using TarotMG.Engine;
using TarotMG.Cards;
using TarotMG.Helpers;

namespace TarotMG.Forms
{
    class MainForm : GameWindow
    {
        private Watermark watermark;
        private Background background;
        private Camera camera;
        private ParticleManager particleManager;
        private Pointer pointer;
        private DeckPlace deckPlace;

        public MainForm(Size size, string title, GameWindowFlags size_mode = GameWindowFlags.Default)
            : base(size.Width, size.Height, GraphicsMode.Default, title, size_mode)
        {
            MouseDown += MainWindow_MouseDown;
            MouseMove += MainWindow_MouseMove;
            MouseUp += MainWindow_MouseUp;
            MouseWheel += MainWindow_MouseWheel;
            KeyDown += MainWindow_KeyDown;

            CardManager.ParseCards();
        }
        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            GL.Enable(EnableCap.Texture2D);
            GL.Enable(EnableCap.CullFace);
            GL.FrontFace(FrontFaceDirection.Ccw);
            GL.CullFace(CullFaceMode.Front);
            GL.Enable(EnableCap.Blend);
            GL.Enable(EnableCap.PointSmooth);
            GL.Enable(EnableCap.LineSmooth);
            GL.Enable(EnableCap.PolygonSmooth);
            GL.Hint(HintTarget.PointSmoothHint, HintMode.Nicest);
            GL.Hint(HintTarget.LineSmoothHint, HintMode.Nicest);
            GL.Hint(HintTarget.PolygonSmoothHint, HintMode.Nicest);

            camera = new Camera(ClientSize);
            background = new Background(Width, Height);
            watermark = new Watermark(Width - 70, Height - 65, 100, 90);
            pointer = new Pointer(0, 0, 70, 70);
            particleManager = new ParticleManager();
            deckPlace = new DeckPlace(-3, 0);
            CardManager.LoadCurrectDeck(deckPlace);

            GuiManager.GuiList.Add(new GuiLoadingIcon("loading_icon", ClientSize, 468, 468, false));
            GuiManager.GuiList.Add(new GuiContextMenu("context_menu", ClientSize, 100, 100, false));
        }
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            GL.Viewport(0, 0, ClientSize.Width, ClientSize.Height);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();

            camera.UpdateMatrices(ClientSize);
            background.Resize(ClientSize.Width, ClientSize.Height);
            watermark.SetPosition(new Vector2(ClientSize.Width - 70, ClientSize.Height - 65));
            GuiManager.WindowResize(ClientSize.Width, ClientSize.Height);
        }
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);
            float time = (float)e.Time;

            CursorVisible = pointer.IsVisible;
            watermark.Update(time);
            GuiManager.Update(time);
            particleManager.Update();
            CardManager.Update(time, camera);
            camera.Update(time);

            Title = $"TarotMG. FPS NOW: {FPSCounter.FPS} :::: MAX FPS: {FPSCounter.Max} :::: MIN FPS: {FPSCounter.Min}";
        }
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            
            #region BEGIN
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.ClearColor(Color.Black);
            #endregion
            #region DRAW 2D
            background.Draw();
            watermark.Draw();
            #endregion
            #region SETUP 3D
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.Normalize);
            GL.Enable(EnableCap.Lighting);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.LoadMatrix(ref camera.ProjectionPerspectiveMatrix);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            GL.LoadMatrix(ref camera.ViewMatrix);
            #endregion
            #region DRAW 3D
            GL.FrontFace(FrontFaceDirection.Cw);
            deckPlace.Draw(camera);
            CardManager.Draw(camera);
            Lighting.Setup();
            GL.FrontFace(FrontFaceDirection.Ccw);
            #endregion
            #region SETUP 2D FOR GUI
            GL.LoadIdentity();
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.LoadMatrix(ref camera.ProjectionOrthographicMatrix);
            GL.Disable(EnableCap.DepthTest);
            GL.Disable(EnableCap.Normalize);
            GL.Disable(EnableCap.Lighting);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            #endregion
            #region DRAW 2D GUI
            GuiManager.Draw();
            CardManager.DrawCardIcons();
            particleManager.Draw();
            pointer.Draw();
            #endregion
            #region END
            GL.LoadIdentity();
            SwapBuffers();
            #endregion

            FPSCounter.Calculate(e.Time);
        }

        private void MainWindow_MouseDown(object sender, MouseButtonEventArgs e)
        {
            pointer.OnMouseDown();
            CardManager.CheckClickOnCardIcon(e.X, e.Y, camera);
            #region card_intersection_test
            bool find = false;
            Ray.Create(e.X, e.Y, camera);
            for (int i = CardManager.CardsCount - 1; i >= 0; i--)
            {
                if (CardManager.Cards[i].RayIntersect(Ray.Start, Ray.Direction))
                {
                    CardManager.SetSelected(CardManager.Cards[i]);
                    find = true;
                    break;
                }
            }
            if (!find)
            {
                CardManager.SelectedCard = null;
            }
            #endregion
            if (e.Button == MouseButton.Left)
            {
                int gui_result = GuiManager.Click(e.X, e.Y);
                if (gui_result >= 0)
                {
                    if (gui_result == 0)
                    {
                        if (WindowState == WindowState.Normal)
                        {
                            WindowState = WindowState.Fullscreen;
                        }
                        else
                        {
                            WindowState = WindowState.Normal;
                        }
                    }
                    else if (gui_result == 5)
                    {
                        System.Diagnostics.Process.Start("https://t.me/tarotmg");
                    }
                    GuiManager.HideContextMenu();
                    return;
                }
                
                if (CardManager.SelectedCard != null)
                {
                    if (Keyboard.GetState().IsKeyDown(Key.LShift) && !CardManager.SelectedCard.NeedToRotate)
                        CardManager.SelectedCard.CreateRotationZ(180);
                }
            }
            else if (e.Button == MouseButton.Right)
            {

                if (CardManager.SelectedCard != null)
                {
                    if (Keyboard.GetState().IsKeyDown(Key.LShift) && !CardManager.SelectedCard.NeedToRotate)
                        CardManager.SelectedCard.CreateRotationY(180);
                }
                else
                {
                    GuiManager.GuiList[1].Position = new Vector2(e.X, e.Y);
                    GuiManager.GuiList[1].IsVisible = !GuiManager.GuiList[1].IsVisible;
                }
            }
            if (e.Button == MouseButton.Middle && e.IsPressed)
            {
                camera.Position = camera.DefaultPosition;
                camera.TargetPosition = camera.DefaultTarget;
                camera.UpdateView();
            }
            particleManager.EngineRocket(e.X + 6, e.Y + 6, 10, 200);
        }
        private void MainWindow_MouseMove(object sender, MouseMoveEventArgs e)
        {
            pointer.OnMouseMove(e.X, e.Y, Width, Height, WindowState);
            particleManager.EngineRocket(e.X + 6, e.Y + 6);

            if (e.Mouse.LeftButton == ButtonState.Pressed)
            {
                if (!Keyboard.GetState().IsKeyDown(Key.LShift) && CardManager.SelectedCard != null)
                {
                    Ray.Create(e.X, e.Y, camera);
                    Vector3 prePos = CardManager.SelectedCard.IntersectionPoint;
                    float planeDist = camera.Position.Z - prePos.Z;
                    Vector3 curPos = -Ray.Direction * (planeDist / Ray.Direction.Z) + camera.Position;
                    CardManager.SelectedCard.IntersectionPoint = curPos;
                    CardManager.SelectedCard.Position += curPos - prePos;
                }
            }
        }
        private void MainWindow_MouseUp(object sender, MouseButtonEventArgs e)
        {
            pointer.OnMouseUp();
            if (CardManager.SelectedCard != null)
            {
                if (deckPlace.CollisionWithCard(camera, CardManager.SelectedCard))
                {
                    CardManager.SelectedCard.Position = new Vector3(deckPlace.Position.X, deckPlace.Position.Y, CardManager.SelectedCard.Position.Z);
                    float original_z = CardManager.Cards[0].Depth;
                    CardManager.Cards.ForEach(c => c.Position = new Vector3(c.Position.X, c.Position.Y, original_z * CardManager.Cards.IndexOf(c)));
                }
                CardManager.SelectedCard = null;
            }
        }
        private void MainWindow_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.GetState().IsKeyDown(Key.LShift))
            {
                CardManager.ScrollCards(deckPlace, e.Delta);
            }
            else
            {
                if (camera.Position.Z - e.Delta > 0 && camera.Position.Z - e.Delta < 50)
                {
                    camera.Position.Z -= e.Delta;
                    camera.UpdateView();
                }
            }
        }

        private void MainWindow_KeyDown(object sender, KeyboardKeyEventArgs e)
        {
            if (e.Key == Key.Escape) Exit();
            if (e.Key == Key.V)
            {
                if (VSync == VSyncMode.On) VSync = VSyncMode.Off;
                else VSync = VSyncMode.On;
            }
            if (e.Key == Key.ControlLeft)
            {
                int margin = 150;
                float move_speed = 0.25f;
                if (pointer.Position.X < margin)
                {
                    camera.Position.X -= move_speed;
                    camera.TargetPosition.X -= move_speed;
                    camera.UpdateView();
                }
                else if (pointer.Position.X > Width - margin)
                {
                    camera.Position.X += move_speed;
                    camera.TargetPosition.X += move_speed;
                    camera.UpdateView();
                }
                if (pointer.Position.Y < margin)
                {
                    camera.Position.Y += move_speed;
                    camera.TargetPosition.Y += move_speed;
                    camera.UpdateView();
                }
                else if (pointer.Position.Y > Height - margin)
                {
                    camera.Position.Y -= move_speed;
                    camera.TargetPosition.Y -= move_speed;
                    camera.UpdateView();
                }
            }
        }
    }
}
