﻿using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace TarotMG.Engine
{
    public static class Ray
    {
        public static Vector3 Start = Vector3.Zero;
        public static Vector3 End = Vector3.Zero;
        public static Vector3 Direction = Vector3.Zero;

        public static void Create(int mouse_x, int mouse_y, Camera camera)
        {
            int[] viewport = new int[4];
            GL.GetInteger(GetPName.Viewport, viewport);

            Start = new Vector3(mouse_x, mouse_y, 0.0f).Unproject(viewport, camera.ProjectionPerspectiveMatrix, camera.ViewMatrix);
            End = new Vector3(mouse_x, mouse_y, 1.0f).Unproject(viewport, camera.ProjectionPerspectiveMatrix, camera.ViewMatrix);

            Direction = End - Start;
            Direction.Normalize();
        }
    }
}
