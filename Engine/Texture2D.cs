﻿using OpenTK.Graphics.OpenGL;
struct Texture2D
{
    public int ID { get; }
    public int Width { get; }
    public int Height { get; }
    public Texture2D(int id, int width, int height)
    {
        ID = id;
        Width = width;
        Height = height;
    }
    public void Bind()
    {
        GL.BindTexture(TextureTarget.Texture2D, ID);
    }
    public void Unbind()
    {
        GL.BindTexture(TextureTarget.Texture2D, 0);
    }
}
