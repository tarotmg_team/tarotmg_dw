﻿using System.Collections.Generic;
using OpenTK;

namespace TarotMG.Engine
{
    public static class Extensions
    {
        public static void MoveItemAtIndexToFront<T>(this List<T> a, int index)
        {
            if (index > 0)
            {
                var tmp = a[a.Count - 1];
                for (var i = a.Count - 1; i != 0; --i) a[i] = a[i - 1];
                a[0] = tmp;
            }
            else
            {
                var temp = a[0];
                for (int i = 0; i < a.Count - 1; i++)
                    a[i] = a[i + 1];
                a[a.Count - 1] = temp;
            }
        }

        /// <summary>
        /// Converts a screen space point into a corresponding point in world space.
        /// </summary>
        /// <param name="coordinate">The coordinate to project</param>
        /// <param name="viewport">The viewport dimensions</param>
        /// <param name="projection">The projection matrix</param>
        /// <param name="modelview">The modelview matrix</param>
        /// <returns>The coordinate in world space.</returns>
        public static Vector3 Unproject(this Vector3 coordinate, int[] viewport, Matrix4 projection, Matrix4 modelview)
        {
            var matrix = Matrix4.Mult(modelview, projection).Inverted();
            var source = new Vector4(
                (coordinate.X - viewport[0]) * 2 / viewport[2] - 1,
                -((coordinate.Y - viewport[1]) * 2 / viewport[3] - 1),
                2 * coordinate.Z - 1,
                1);
            var vector = Vector4.Transform(source, matrix);
            if (vector.W < 0.00001) return new Vector3(1000000, 1000000, 1000000);
            var result = Vector3.Divide(new Vector3(vector.X, vector.Y, vector.Z), vector.W);
            return result;
        }

        /// <summary>
        /// Projects a coordinate from world space into screen space.
        /// </summary>
        /// <param name="coordinate">The coordinate to project</param>
        /// <param name="viewport">The viewport dimensions</param>
        /// <param name="projection">The projection matrix</param>
        /// <param name="modelview">The modelview matrix</param>
        /// <returns>The coordinate in screen space.</returns>
        public static Vector3 Project(this Vector3 coordinate, int[] viewport, Matrix4 projection, Matrix4 modelview)
        {
            var source = new Vector4(coordinate.X, coordinate.Y, coordinate.Z, 1);
            var imed = Vector4.Transform(source, modelview);
            var vector = Vector4.Transform(imed, projection);
            if (vector.W < 0.00001) return new Vector3(1000000, 1000000, 1000000);
            var result = Vector3.Divide(new Vector3(vector.X, -vector.Y, vector.Z), vector.W);
            result.X = viewport[0] + viewport[2] * (result.X + 1) / 2;
            result.Y = viewport[1] + viewport[3] * (result.Y + 1) / 2;
            result.Z = (result.Z + 1) / 2;
            return result;
        }
    }
}
