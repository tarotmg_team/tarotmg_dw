﻿using System;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Assimp;
using TarotMG.Helpers;

namespace TarotMG.Engine
{
    class Object3D
    {
        public Scene model;
        public Vector3 objectCenter, objectMin, objectMax;
        public Matrix4 translationMatrix = Matrix4.Identity;
        public Matrix4 rotationMatrix = Matrix4.Identity;
        public Matrix4 scaleMatrix = Matrix4.Identity;
        public Matrix4 Object3DMatrix
        {
            get
            {
                return (scaleMatrix * rotationMatrix) * translationMatrix;
            }
        }
        public float Depth
        {
            get
            {
                return objectMax.Z - objectMin.Z;
            }
        }
        public float Width
        {
            get
            {
                return objectMax.X - objectMin.X;
            }
        }
        public float Height
        {
            get
            {
                return objectMax.Y - objectMin.Y;
            }
        }

        public Object3D(Scene model, bool genBounds) {
            this.model = model;
            if (genBounds) GenerateBoundingBox();
        }
        public void GenerateBoundingBox()
        {
            objectMin = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);
            objectMax = new Vector3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity);
            Matrix4 identity = Matrix4.Identity;

            ComputeBoundingBox(model.RootNode, ref objectMin, ref objectMax, ref identity);

            objectCenter.X = (objectMin.X + objectMax.X) / 2.0f;
            objectCenter.Y = (objectMin.Y + objectMax.Y) / 2.0f;
            objectCenter.Z = (objectMin.Z + objectMax.Z) / 2.0f;
        }
        private void ComputeBoundingBox(Node node, ref Vector3 min, ref Vector3 max, ref Matrix4 trafo)
        {
            Matrix4 prev = trafo;
            trafo = Matrix4.Mult(prev, Helper.AssimpToOpenTKMatrix(node.Transform));

            if (node.HasMeshes)
            {
                foreach (int index in node.MeshIndices)
                {
                    Mesh mesh = model.Meshes[index];
                    for (int i = 0; i < mesh.VertexCount; i++)
                    {
                        Vector3 tmp = Helper.AssimpToOpenTKVector(mesh.Vertices[i]);
                        Vector3.TransformVector(ref tmp, ref trafo, out tmp);

                        min.X = Math.Min(min.X, tmp.X);
                        min.Y = Math.Min(min.Y, tmp.Y);
                        min.Z = Math.Min(min.Z, tmp.Z);

                        max.X = Math.Max(max.X, tmp.X);
                        max.Y = Math.Max(max.Y, tmp.Y);
                        max.Z = Math.Max(max.Z, tmp.Z);
                    }
                }
            }

            for (int i = 0; i < node.ChildCount; i++)
            {
                ComputeBoundingBox(node.Children[i], ref min, ref max, ref trafo);
            }
            trafo = prev;
        }
        public void Draw(Camera camera)
        {
            GL.LoadIdentity();
            Matrix4 m = Object3DMatrix * camera.ViewMatrix;
            GL.LoadMatrix(ref m);
            RecursiveRender(model, model.RootNode);
        }
        private void RecursiveRender(Scene scene, Node node)
        {
            Matrix4 m = Helper.AssimpToOpenTKMatrix(node.Transform);
            m.Transpose();
            GL.PushMatrix();
            GL.MultMatrix(ref m);

            if (node.HasMeshes)
            {
                foreach (int index in node.MeshIndices)
                {
                    Mesh mesh = scene.Meshes[index];

                    foreach (Face face in mesh.Faces)
                    {
                        OpenTK.Graphics.OpenGL.PrimitiveType faceMode;
                        switch (face.IndexCount)
                        {
                            case 1:
                                faceMode = OpenTK.Graphics.OpenGL.PrimitiveType.Points;
                                break;
                            case 2:
                                faceMode = OpenTK.Graphics.OpenGL.PrimitiveType.Lines;
                                break;
                            case 3:
                                faceMode = OpenTK.Graphics.OpenGL.PrimitiveType.Triangles;
                                break;
                            default:
                                faceMode = OpenTK.Graphics.OpenGL.PrimitiveType.Polygon;
                                break;
                        }

                        GL.Begin(faceMode);
                        for (int i = 0; i < face.IndexCount; i++)
                        {
                            int indice = face.Indices[i];
                            if (mesh.HasVertexColors(0))
                            {
                                Color4 vertColor = Helper.AssimpToOpenTKColor(mesh.VertexColorChannels[0][indice]);
                            }
                            if (mesh.HasNormals)
                            {
                                Vector3 normal = Helper.AssimpToOpenTKVector(mesh.Normals[indice]);
                                GL.Normal3(normal);
                            }
                            if (mesh.HasTextureCoords(0))
                            {
                                Vector3 uvw = Helper.AssimpToOpenTKVector(mesh.TextureCoordinateChannels[0][indice]);
                                GL.TexCoord2(uvw.X, 1 - uvw.Y);
                            }
                            Vector3 pos = Helper.AssimpToOpenTKVector(mesh.Vertices[indice]);
                            GL.Vertex3(pos);
                        }
                        GL.End();
                    }
                }
            }

            for (int i = 0; i < node.ChildCount; i++)
            {
                RecursiveRender(model, node.Children[i]);
            }
        }
    }
}
