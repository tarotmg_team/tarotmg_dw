﻿using OpenTK.Graphics.OpenGL;

namespace TarotMG.Engine
{
    public static class Lighting
    {
        public static void Setup()
        {
            GL.Disable(EnableCap.Light0);
            GL.LoadIdentity();
            float[] matDiffuse = { 1.0f, 1.0f, 1.0f, 1.0f };
            float[] matSpecular = { 1.0f, 1.0f, 1.0f, 1.0f };
            float[] lightDiffuse = { 1.0f, 1.0f, 1.0f, 1.0f };
            float[] lightPosition = { 0, 0, 0, 1.0f };
            float lightShininess = 10.0f;
            GL.Material(MaterialFace.Front, MaterialParameter.Diffuse, matDiffuse);
            GL.Material(MaterialFace.Front, MaterialParameter.Specular, matSpecular);
            GL.Material(MaterialFace.Front, MaterialParameter.Shininess, lightShininess);
            GL.Light(LightName.Light0, LightParameter.Diffuse, matDiffuse);
            GL.Light(LightName.Light0, LightParameter.Position, lightPosition);
            GL.Enable(EnableCap.Light0);
        }
    }
}
