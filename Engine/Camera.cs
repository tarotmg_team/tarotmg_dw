﻿using System.Drawing;
using OpenTK;

namespace TarotMG.Engine
{
    public class Camera
    {
        public float FOV;
        public float Ratio;
        public float NearPlane;
        public float FarPlane;
        public Matrix4 ProjectionPerspectiveMatrix;
        public Matrix4 ProjectionOrthographicMatrix;
        public Matrix4 ViewMatrix;
        public Vector3 Position;
        public Vector3 TargetPosition;
        public Vector3 Up;
        public Vector3 DefaultPosition = new Vector3(0, 0, 8);
        public Vector3 DefaultTarget = Vector3.Zero;

        private bool needToMove = false;
        private Vector3 movePoint;
        private Vector3 moveDirection;
        private float preMoveDist = float.MaxValue;
        
        public Camera(Size clientSize)
        {
            FOV = MathHelper.PiOver4;
            Ratio = (float)clientSize.Width / clientSize.Height;
            NearPlane = 1.0f;
            FarPlane = 1000.0f;

            Position = DefaultPosition;
            TargetPosition = DefaultTarget;
            Up = Vector3.UnitY;

            UpdateMatrices(clientSize);
        }
        public void SetMovePoint(Vector3 point)
        {
            movePoint = point;
            moveDirection = movePoint - Position;
            needToMove = true;
        }
        public void Update(float time)
        {
            if (needToMove)
            {
                float dist = Vector2.Distance(Position.Xy, movePoint.Xy);
                if (dist < preMoveDist)
                {
                    preMoveDist = dist;
                    Vector3 step = moveDirection * time * 2;
                    Position += new Vector3(step.X, step.Y, 0);
                    TargetPosition += new Vector3(step.X, step.Y, 0);
                    UpdateView();
                }
                else
                {
                    needToMove = false;
                    preMoveDist = float.MaxValue;
                }
            }
        }
        public void UpdateMatrices(Size clientSize)
        {
            ProjectionPerspectiveMatrix = Matrix4.CreatePerspectiveFieldOfView(FOV, Ratio, NearPlane, FarPlane);
            ProjectionOrthographicMatrix = Matrix4.CreateOrthographicOffCenter(0, clientSize.Width, clientSize.Height, 0, -1, +1);
            ViewMatrix = Matrix4.LookAt(Position, TargetPosition, Up);
        }
        public void UpdateView()
        {
            ViewMatrix = Matrix4.LookAt(Position, TargetPosition, Up);
        }
    }
}
