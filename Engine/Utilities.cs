﻿using System.IO;
using System.Drawing;
using System.Globalization;
using System.Drawing.Imaging;
using OpenTK.Graphics.OpenGL;
using Assimp;

namespace TarotMG.Engine
{
    class Utilities
    {
        private static NumberFormatInfo formatInfo = new CultureInfo("en-US").NumberFormat;

        public static Texture2D LoadTexture(string name)
        {
            Bitmap bmp;
            int id = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, id);
            if (File.Exists(@"Data/Textures/" + name)) { bmp = (Bitmap)Image.FromFile(@"Data/Textures/" + name); }
            else { if (File.Exists(name)) { bmp = (Bitmap)Image.FromFile(name); } else { throw new FileNotFoundException("File not found: " + name); } }
            BitmapData data = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
            bmp.UnlockBits(data);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)OpenTK.Graphics.OpenGL.TextureWrapMode.Clamp);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)OpenTK.Graphics.OpenGL.TextureWrapMode.Clamp);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

            return new Texture2D(id, bmp.Width, bmp.Height);
        }

        public static Texture2D LoadTexture(Image image)
        {
            Bitmap bmp = (Bitmap)image;
            int id = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, id);
            BitmapData data = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
            bmp.UnlockBits(data);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)OpenTK.Graphics.OpenGL.TextureWrapMode.Clamp);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)OpenTK.Graphics.OpenGL.TextureWrapMode.Clamp);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

            return new Texture2D(id, bmp.Width, bmp.Height);
        }

        public static Object3D LoadObjModel(string name)
        {
            string path = $"Data/Models/{name}";
            bool flipUV = false;
            var importer = new AssimpContext();
            if (!importer.IsImportFormatSupported(Path.GetExtension(path)))
            { throw new FileNotFoundException("File not found at 'Data/Models/" + name + "'"); }
            var postProcessFlags =
                PostProcessSteps.CalculateTangentSpace |
                PostProcessSteps.Triangulate |
                PostProcessSteps.JoinIdenticalVertices |
                PostProcessSteps.SortByPrimitiveType;
            if (flipUV) { postProcessFlags |= PostProcessSteps.FlipUVs; }
            var model = importer.ImportFile(path, postProcessFlags);

            return new Object3D(model, true);
        }

        public static Object3D LoadCardModel(string deck_name)
        {
            string path = $"Data/Decks/{deck_name}/card.obj";
            bool flipUV = false;
            var importer = new AssimpContext();
            if (!importer.IsImportFormatSupported(Path.GetExtension(path)))
            { throw new FileNotFoundException($"Card model not found at 'Data/Decks/{deck_name}/'"); }
            var postProcessFlags =
                PostProcessSteps.CalculateTangentSpace |
                PostProcessSteps.Triangulate |
                PostProcessSteps.JoinIdenticalVertices |
                PostProcessSteps.SortByPrimitiveType;
            if (flipUV) { postProcessFlags |= PostProcessSteps.FlipUVs; }
            var model = importer.ImportFile(path, postProcessFlags);

            return new Object3D(model, true);
        }
    }
}
