﻿using System;
using System.Drawing;
using OpenTK;
using TarotMG.Engine;

namespace TarotMG.Cards
{
    class Card
    {
        private Object3D object3d;

        public Texture2D Texture;
        public string DeckName;
        public string Name;
        public Vector3 Position
        {
            get
            {
                return object3d.translationMatrix.ExtractTranslation();
            }
            set
            {
                object3d.translationMatrix = Matrix4.CreateTranslation(value);
            }
        }
        public Matrix4 CardMatrix
        {
            get
            {
                return object3d.Object3DMatrix;
            }
        }
        public Vector3 IntersectionPoint;
        public Vector3 Min
        {
            get
            {
                return Vector3.TransformPosition(object3d.objectMin, object3d.translationMatrix);
            }
        }
        public Vector3 Max
        {
            get
            {
                return Vector3.TransformPosition(object3d.objectMax, object3d.translationMatrix);
            }
        }
        public bool IsVisible;
        public CardIcon cardIcon;
        public float Depth
        {
            get { return object3d.Depth; }
        }
        public float Width
        {
            get { return object3d.Width; }
        }
        public float Height
        {
            get { return object3d.Height; }
        }

        public bool NeedToRotate { get; private set; } = false;
        public bool NeedToMove { get; private set; } = false;
        private bool needToUp = false;
        private bool needToDown = false;
        private bool needToRot = false;
        private Vector3 rotAxis;
        private float rotAngle;
        private Matrix4 curRM = Matrix4.Identity;
        private Matrix4 origRM = Matrix4.Identity;
        private Matrix4 origTM = Matrix4.Identity;
        private float sumA = 1f;
        private float sumZ = 0.1f;
        private float pointZ = 1.0f;

        public Card(string deck, string name, Image texture, Object3D model)
        {
            DeckName = deck;
            Name = name;
            Texture = Utilities.LoadTexture(texture);
            object3d = model;
            cardIcon = new CardIcon(this);
            Position = new Vector3(0, 0, -Width / 2.0f);
        }
        public void Update(float time)
        {
            if (NeedToRotate)
            {
                if (needToUp)
                {
                    Matrix4 m = Matrix4.CreateTranslation(0, 0, sumZ);
                    object3d.translationMatrix = origTM * m;

                    if (sumZ <= pointZ)
                    {
                        sumZ += 10 * time;
                    }
                    else
                    {
                        object3d.translationMatrix = origTM * Matrix4.CreateTranslation(0, 0, pointZ);
                        needToUp = false;
                        needToRot = true;
                        needToDown = false;
                        origTM = object3d.translationMatrix;
                        sumZ = -0.1f;
                    }
                }
                if (needToRot)
                {
                    Matrix4 m = Matrix4.CreateFromAxisAngle(rotAxis, MathHelper.DegreesToRadians(sumA));
                    object3d.rotationMatrix = origRM * m;

                    if (rotAngle - sumA < 1)
                    {
                        object3d.rotationMatrix = origRM * Matrix4.CreateFromAxisAngle(rotAxis, MathHelper.DegreesToRadians(rotAngle));
                        needToUp = false;
                        needToRot = false;
                        needToDown = true;
                    }
                    else
                    {
                        sumA += 180 * time;
                    }
                }
                if (needToDown)
                {
                    Matrix4 m = Matrix4.CreateTranslation(0, 0, sumZ);
                    object3d.translationMatrix = origTM * m;

                    sumA = 0;

                    if (sumZ >= -pointZ)
                    {
                        sumZ -= 10 * time;
                    }
                    else
                    {
                        object3d.translationMatrix = origTM * Matrix4.CreateTranslation(0, 0, -pointZ);
                        needToDown = false;
                        needToUp = false;
                        needToRot = false;
                        NeedToRotate = false;
                    }
                }
            }
            else if (NeedToMove)
            {

            }
        }
        public void CreateRotationX(float angle)
        {
            rotAxis = Vector3.UnitX;
            SetRotParams(angle);
        }
        public void CreateRotationY(float angle)
        {
            rotAxis = Vector3.UnitY;
            SetRotParams(angle);
        }
        public void CreateRotationZ(float angle)
        {
            rotAxis = Vector3.UnitZ;
            SetRotParams(angle);
        }
        private void SetRotParams(float angle)
        {
            sumA = 1f;
            sumZ = 0.1f;
            rotAngle = angle;

            curRM = Matrix4.CreateFromAxisAngle(rotAxis, MathHelper.DegreesToRadians(rotAngle));
            origRM = object3d.rotationMatrix;
            
            origTM = object3d.translationMatrix;

            NeedToRotate = true;
            needToUp = true;
            needToRot = false;
            needToDown = false;
        }
        public void IncrementPosition(Vector3 position)
        {
            Position += position;
        }
        public bool RayIntersect(Vector3 rayStart, Vector3 rayDir)
        {
            Vector3 cmin = Min;
            Vector3 cmax = Max;

            Vector3 rs = rayStart;
            Vector3 rd = rayDir;

            Vector3 diff0 = cmin - rs;
            Vector3 diff1 = cmax - rs;
            Vector3 t0 = new Vector3(diff0.X / rd.X, diff0.Y / rd.Y, diff0.Z / rd.Z);
            Vector3 t1 = new Vector3(diff1.X / rd.X, diff1.Y / rd.Y, diff1.Z / rd.Z);
            Vector3 tMin = new Vector3(Math.Min(t0.X, t1.X), Math.Min(t0.Y, t1.Y), Math.Min(t0.Z, t1.Z));
            Vector3 tMax = new Vector3(Math.Max(t0.X, t1.X), Math.Max(t0.Y, t1.Y), Math.Max(t0.Z, t1.Z));
            float tnear = Math.Max(Math.Max(tMin.X, tMin.Y), tMin.Z);
            float tfar = Math.Min(Math.Min(tMax.X, tMax.Y), tMax.Z);

            if (tnear <= tfar)
            {
                IntersectionPoint = rd * tnear + rs;
                return true;
            }
            return false;
        }
        public void Draw(Camera camera)
        {
            Texture.Bind();
            object3d.Draw(camera);
            Texture.Unbind();
        }
    }
}
