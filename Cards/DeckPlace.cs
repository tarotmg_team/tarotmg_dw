﻿using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using TarotMG.Engine;

namespace TarotMG.Cards
{
    class DeckPlace
    {
        public Object3D Obj3D;
        public Vector3 Position
        {
            get
            {
                return Obj3D.translationMatrix.ExtractTranslation();
            }
            set
            {
                Obj3D.translationMatrix = Matrix4.CreateTranslation(value);
            }
        }
        private Texture2D texture;

        public DeckPlace(float x, float y)
        {
            Obj3D = Utilities.LoadObjModel("DeckPlace.obj");
            texture = Utilities.LoadTexture("DeckPlace.png");
            float z = -Obj3D.Depth;
            Obj3D.translationMatrix = Matrix4.CreateTranslation(x, y, z);
        }
        public void Draw(Camera camera)
        {
            texture.Bind();
            Obj3D.Draw(camera);
            texture.Unbind();
        }
        public bool CollisionWithCard(Camera camera, Card card)
        {
            int[] viewport = new int[4];
            GL.GetInteger(GetPName.Viewport, viewport);

            Vector3 card_min = card.Min.Project(viewport, camera.ProjectionPerspectiveMatrix, camera.ViewMatrix);
            Vector3 card_max = card.Max.Project(viewport, camera.ProjectionPerspectiveMatrix, camera.ViewMatrix);
            Vector3 deck_max = Vector3.TransformPosition(Obj3D.objectMax, Obj3D.translationMatrix);
            Vector3 deck_min = Vector3.TransformPosition(Obj3D.objectMin, Obj3D.translationMatrix);
            deck_max = deck_max.Project(viewport, camera.ProjectionPerspectiveMatrix, camera.ViewMatrix);
            deck_min = deck_min.Project(viewport, camera.ProjectionPerspectiveMatrix, camera.ViewMatrix);

            float rect_x = card_min.X;
            float rect_y = card_max.Y;
            float rect_w = card_max.X - card_min.X;
            float rect_h = card_min.Y - card_max.Y;
            RectangleF card_rect = new RectangleF(rect_x, rect_y, rect_w, rect_h);

            rect_x = deck_min.X;
            rect_y = deck_max.Y;
            rect_w = deck_max.X - deck_min.X;
            rect_h = deck_min.Y - deck_max.Y;
            RectangleF deck_rect = new RectangleF(rect_x, rect_y, rect_w, rect_h);
            
            return card_rect.IntersectsWith(deck_rect);
        }
    }
}
