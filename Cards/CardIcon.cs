﻿using System;
using System.Drawing;
using System.Linq;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace TarotMG.Cards
{
    class CardIcon
    {
        private uint vertexBufferId;
        private uint textureBufferId;
        private float[] vertexData;
        private Texture2D texture;
        private Card card;

        public string Name;
        public Vector2 Position;
        public int Width { get; private set; }
        public int Height { get; private set; }
        public bool IsVisible;

        public CardIcon(Card card, bool visible = false)
        {
            this.card = card;
            Name = card.Name;
            Width = 25;
            Height = 50;
            IsVisible = visible;
            texture = card.Texture;
            Position = Vector2.Zero;
            #region SETUP VALUES
            // Set-up vertex buffer
            vertexData = new float[] {
                Position.X - Width/2f, Position.Y - Height/2f, 0.0f,    //LD
                Position.X + Width/2f, Position.Y - Height/2f, 0.0f,    //RD
                Position.X + Width/2f, Position.Y + Height/2f, 0.0f,    //RU
                Position.X - Width/2f, Position.Y + Height/2f, 0.0f,    //LU
            };

            // Set-up texture buffer
            float[] textureData = new float[] {
                0.0f, 0.0f, // LU
                1.0f, 0.0f, // RU
                1.0f, 0.5f, // RD
                0.0f, 0.5f, // LD
            };
            #endregion
            #region GENERATE BUFFERS
            // Vertex buffer generate
            GL.GenBuffers(1, out vertexBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertexData.Length * sizeof(float)), vertexData, BufferUsageHint.StaticDraw);
            // Texture buffer generate
            GL.GenBuffers(1, out textureBufferId);
            GL.BindBuffer(BufferTarget.ArrayBuffer, textureBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(textureData.Length * sizeof(float)), textureData, BufferUsageHint.StaticDraw);
            #endregion
        }

        public void Draw()
        {
            if (IsVisible)
            {
                // Bind texture buffer
                GL.BindBuffer(BufferTarget.ArrayBuffer, textureBufferId);
                GL.TexCoordPointer(2, TexCoordPointerType.Float, 0, IntPtr.Zero);
                // Bind vertex buffer
                GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
                GL.VertexPointer(3, VertexPointerType.Float, 0, IntPtr.Zero);
                EnableStates();

                GL.LoadIdentity();
                GL.Translate(new Vector3(Position));
                texture.Bind();
                GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Count());
                texture.Unbind();

                DisableStates();
            }
        }
        private void EnableStates()
        {
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
            GL.Enable(EnableCap.AlphaTest);
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);
        }
        private void DisableStates()
        {
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);
            GL.Disable(EnableCap.AlphaTest);
            GL.Disable(EnableCap.Blend);
        }       
        public bool IntersectionTest(int x, int y)
        {
            return new RectangleF(Position.X - Width / 2, Position.Y - Height / 2, Width, Height).Contains(x, y);
        }
    }
}
