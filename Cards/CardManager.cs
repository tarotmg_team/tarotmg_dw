﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using TarotMG.Engine;

namespace TarotMG.Cards
{
    class CardManager
    {
        public static string CurrentDeck = "THOTH";
        public static List<Card> AllCards = new List<Card>();
        public static List<Card> Cards = new List<Card>();
        public static Card SelectedCard = null;
        public static int CardsCount
        {
            get
            {
                return Cards.Count;
            }
        }

        public static void ParseCards()
        {
            AllCards = new List<Card>();
            List<Image> imgList = new List<Image>();
            string decks_path = @"Data\Decks\";
            string[] dirs = Directory.GetDirectories(decks_path);
            foreach (string dir in dirs)
            {
                string[] fs = Directory.GetFiles(dir);
                foreach (string f in fs)
                {
                    if (Path.GetExtension(f) != ".obj")
                    {
                        Image image = Image.FromFile(f);
                        image.Tag = Path.GetFileNameWithoutExtension(f);
                        imgList.Add(image);
                    }
                }

                foreach (var img in imgList)
                {
                    string deck_name = new DirectoryInfo(dir).Name;
                    string card_name = (string)img.Tag;
                    Object3D obj3d = Utilities.LoadCardModel(deck_name);
                    Card card = new Card(deck_name, card_name, img, obj3d);
                    AllCards.Add(card);
                }

                imgList.Clear();
            }
        }
        public static void LoadCurrectDeck(DeckPlace deckPlace)
        {
            Cards.Clear();
            Cards.AddRange(AllCards.FindAll(c => c.DeckName == CurrentDeck));
            float original_z = Cards[0].Depth;
            Cards.ForEach(c => c.Position = new Vector3(deckPlace.Position.X, deckPlace.Position.Y, original_z * Cards.IndexOf(c)));
        }
        public static void Draw(Camera camera)
        {
            Cards.ForEach(c => c.Draw(camera));
        }
        public static void SetSelected(Card card)
        {
            SelectedCard = Cards.Find(c => c == card);
            if (SelectedCard != null && !SelectedCard.NeedToRotate)
            {
                int selected_index = Cards.IndexOf(SelectedCard);
                Cards.RemoveAt(selected_index);
                Cards.Insert(CardsCount, SelectedCard);

                float original_z = Cards[0].Depth;
                Cards.ForEach(c => c.Position = new Vector3(c.Position.X, c.Position.Y, original_z * Cards.IndexOf(c)));
            }
        }
        public static void ScrollCards(DeckPlace deckPlace, int delta)
        {
            float original_z = Cards[0].Depth;
            List<Card> tmp_list = Cards.FindAll(c => c.Position.Xy == deckPlace.Position.Xy);
            if (tmp_list.Count < Cards.Count)
            {
                Cards.RemoveAll(c => c.Position.Xy == deckPlace.Position.Xy);
                tmp_list.MoveItemAtIndexToFront(delta);
                Cards.AddRange(tmp_list);
            }
            else
            {
                Cards.MoveItemAtIndexToFront(delta);
            }
            Cards.ForEach(c => c.Position = new Vector3(c.Position.X, c.Position.Y, original_z * Cards.IndexOf(c)));
        }
        public static void CheckVisibility(Camera camera)
        {
            foreach (var c in Cards)
            {
                int[] viewport = new int[4];
                GL.GetInteger(GetPName.Viewport, viewport);
                Vector3 pos = c.Position.Project(viewport, camera.ProjectionPerspectiveMatrix, camera.ViewMatrix);
                Vector3 min = c.Min.Project(viewport, camera.ProjectionPerspectiveMatrix, camera.ViewMatrix);
                Vector3 max = c.Max.Project(viewport, camera.ProjectionPerspectiveMatrix, camera.ViewMatrix);

                bool visible = true;
                float x = pos.X;
                float y = pos.Y;

                if (max.X < 0)
                {
                    visible = false;
                    x = 0 + c.cardIcon.Width / 2;
                }
                if (min.X > viewport[2])
                {
                    visible = false;
                    x = viewport[2] - c.cardIcon.Width / 2;
                }
                if (min.Y < 0)
                {
                    visible = false;
                    y = 0 + c.cardIcon.Height / 2;
                }
                if (max.Y > viewport[3])
                {
                    visible = false;
                    y = viewport[3] - c.cardIcon.Height / 2;
                }

                c.IsVisible = visible;
                c.cardIcon.IsVisible = !visible;

                if (!visible)
                {
                    c.cardIcon.Position = new Vector2(x, y);
                }
            }
        }
        public static void DrawCardIcons()
        {
            Cards.FindAll(c => c.IsVisible == false).ForEach(c => c.cardIcon.Draw());
        }
        public static void CheckClickOnCardIcon(int x, int y, Camera camera)
        {
            var invCards = Cards.FindAll(c => c.IsVisible == false);
            foreach (Card c in invCards)
            {
                if (c.cardIcon.IntersectionTest(x, y))
                {
                    camera.SetMovePoint(c.Position);
                    return;
                }
            }
        }
        public static void Update(float time, Camera camera)
        {
            Cards.ForEach(c => c.Update(time));
            CheckVisibility(camera);
        }
    }
}
