﻿using OpenTK;
using TarotMG.Engine;

namespace TarotMG.Cards
{
    class CardPlace
    {
        public Object3D Obj3D;
        private Texture2D texture;

        public CardPlace(float x, float y, float z = -0.6f)
        {
            Obj3D = Utilities.LoadObjModel("DeckPlace.obj");
            texture = Utilities.LoadTexture("CardPlace.png");
            Obj3D.translationMatrix = Matrix4.CreateTranslation(x, y, z);
        }

        public void Draw(Camera camera)
        {
            texture.Bind();
            Obj3D.Draw(camera);
            texture.Unbind();
        }
    }
}
