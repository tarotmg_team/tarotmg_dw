﻿using System.Drawing;
using OpenTK;
using TarotMG.Forms;

namespace TarotMG
{
    class Program
    {
        static void Main(string[] args)
        {
            using (MainForm mf = new MainForm(new Size(1000, 600), "TarotMG", GameWindowFlags.Default))
            {
                mf.VSync = VSyncMode.Off;
                mf.Run(100, 100);
            }
        }
    }
}
